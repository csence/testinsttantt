﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApiFlujo.Domain.Model
{
   public class Respuesta
    {
        public int Id { get; set; }
        public int IdFlujoPasosXcampo { get; set; }
        public string DescRespuesta { get; set; }
        public int IdUsuario { get; set; }
    }
}
