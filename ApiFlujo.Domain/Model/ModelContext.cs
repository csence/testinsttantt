﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApiFlujo.Domain.Model
{
    public partial class ModelContext : DbContext
    {


        public ModelContext(DbContextOptions<ModelContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Flujos> flujos { get; set; }
        public virtual DbSet<Pasos> pasos { get; set; }
        public virtual DbSet<Campos> campos { get; set; }
        public virtual DbSet<FlujoPasosXCampo> FlujoPasosXCampo { get; set; }
        public virtual DbSet<FlujoXPaso> flujoXpasos { get; set; }
        public virtual DbSet<Respuesta> respuesta { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Flujos>(entity =>
            {
                entity.HasKey(e => e.Id).HasName("Id");
                entity.ToTable("Flujos");
                entity.Property(e => e.Descripcion)
                .HasColumnName("Descripcion")
                .HasColumnType("nvarchar(100)");
                entity.Property(e => e.Codigo)
             .HasColumnName("Codigo")
             .HasColumnType("nvarchar(100)");



            });
            modelBuilder.Entity<Pasos>(entity =>
            {
                entity.HasKey(e => e.Id).HasName("Id");
                entity.ToTable("Pasos");
                entity.Property(e => e.Descripcion)
                .HasColumnName("Descripcion")
                .HasColumnType("nvarchar(100)");
                entity.Property(e => e.Codigo)
             .HasColumnName("Codigo")
             .HasColumnType("nvarchar(100)");



            });
            modelBuilder.Entity<Campos>(entity =>
            {
                entity.HasKey(e => e.Id).HasName("Id");
                entity.ToTable("Campos");
                entity.Property(e => e.Descripcion)
                 .HasColumnName("Descripcion")
                 .HasColumnType("nvarchar(100)");
                entity.Property(e => e.Codigo)
             .HasColumnName("Codigo")
             .HasColumnType("nvarchar(100)");


            });

            modelBuilder.Entity<FlujoXPaso>(entity =>
            {
                entity.HasKey(e => e.Id).HasName("Id");
                entity.ToTable("FlujoXPaso");
                entity.Property(e => e.IdFlujo)
                 .HasColumnName("IdFlujo")
                 .HasColumnType("int");
                entity.Property(e => e.IdPaso)
             .HasColumnName("IdPaso")
             .HasColumnType("int");

              


            });

            modelBuilder.Entity<FlujoPasosXCampo>(entity =>
            {
                entity.HasKey(e => e.Id).HasName("Id");
                entity.ToTable("FlujoPasosXCampo");
                entity.Property(e => e.IdCampo)
                 .HasColumnName("IdCampo")
                 .HasColumnType("int");
                entity.Property(e => e.IdFlujoXPaso)
             .HasColumnName("IdFlujoXPaso")
             .HasColumnType("int");


            });

            modelBuilder.Entity<Respuesta>(entity =>
            {
                entity.HasKey(e => e.Id).HasName("Id");
                entity.ToTable("Respuesta");
                entity.Property(e => e.DescRespuesta)
                .HasColumnName("DescRespuesta")
                .HasColumnType("nvarchar(100)");
                entity.Property(e => e.IdFlujoPasosXcampo)
             .HasColumnName("IdFlujoPasosXcampo")
             .HasColumnType("int)");
                entity.Property(e => e.IdUsuario)
            .HasColumnName("IdUsuario")
            .HasColumnType("int)");



            });

            OnModelCreatingPartial(modelBuilder);
        }
        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }

}
