﻿using ApiFlujo.Domain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApiFlujo.Domain.Dto
{
   public class DtoFlujoxPaso
    {
        public Flujos flujo { get; set; }
        public FlujoXPaso flujoxpaso { get; set; }
        public Pasos paso { get; set; }
        //public int IdFlujo { get; set; }
        //public string DescFlujo { get; set; }
        //public string CodigoFlujo { get; set; }
        //public int idFlujoXPaso { get; set; }
        //public int IdPaso { get; set; }
        //public string DescPaso { get; set; }
        //public string CodigoPaso { get; set; }
    }
}
