﻿using ApiFlujo.Domain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApiFlujo.Domain.Dto
{
   public class PasoXCampoXRespuesta
    {
        public Pasos paso { get; set; }
        public Campos campo { get; set; }
        public Respuesta respuesta { get; set; }
        public FlujoPasosXCampo flujopasoxcampo { get; set; }

    }
}
