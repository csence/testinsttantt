﻿using ApiFlujo.Domain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApiFlujo.Domain.Dto
{
   public class DtoCampos
    {
        public int IdFlujoXpaso { get; set; }
        public int IdFlujo { get; set; }
        public int IdUsuario { get; set; }

        public  List<Respuesta> Listrespuesta { get; set; }
    }
}
