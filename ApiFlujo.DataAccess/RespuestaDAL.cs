﻿using ApiFlujo.Domain.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using ApiFlujo.Domain.Dto;


namespace ApiFlujo.DataAccess
{
 public   class RespuestaDAL
    {
        private readonly ModelContext context;
        public RespuestaDAL(ModelContext _context)
        {
            context = _context;
        }

        public async Task<List<PasoXCampoXRespuesta>> GuardarRespuesta(DtoCampos obj) {

            foreach (var item in obj.Listrespuesta)
            {
                var consultaRespuesta = context.respuesta.Where(x=> x.IdFlujoPasosXcampo == item.IdFlujoPasosXcampo  && x.IdUsuario == item.IdUsuario).FirstOrDefault();

                if (consultaRespuesta != null)
                {
                    consultaRespuesta.DescRespuesta = item.DescRespuesta;
                }
                else {
                    var objRespuesta = context.respuesta.Add(item);
                }
            }

            context.SaveChanges();

            var Respuesta = await this.ObtenerCampos(obj);

            return Respuesta;

        }

    


        public async Task<List<PasoXCampoXRespuesta>> ObtenerCampos(DtoCampos obj)
        {
            try
            {
                var ObjRespuesta = (from p in context.FlujoPasosXCampo
                                    join e in context.flujoXpasos
                                    on p.IdFlujoXPaso equals e.Id
                                    join a in context.pasos
                                    on e.IdPaso equals a.Id
                                    join c in context.campos
                                    on p.IdCampo equals c.Id
                                    join d in context.respuesta
                                    on p.Id equals d.IdFlujoPasosXcampo
                                    where d.IdUsuario == obj.IdUsuario
                                    && e.IdFlujo == obj.IdFlujo
                                    && e.Id == obj.IdFlujoXpaso
                                    select new PasoXCampoXRespuesta
                                    {
                                        flujopasoxcampo = p,
                                        respuesta = d,
                                        campo = c

                                    }).ToList();
                if (ObjRespuesta.Count() == 0)
                {
                    var Respuesta = (from p in context.FlujoPasosXCampo
                                     join e in context.flujoXpasos
                                     on p.IdFlujoXPaso equals e.Id
                                     join a in context.pasos
                                     on e.IdPaso equals a.Id
                                     join c in context.campos
                                     on p.IdCampo equals c.Id
                                     where
                                       e.IdFlujo == obj.IdFlujo
                                     && e.Id == obj.IdFlujoXpaso
                                     select new PasoXCampoXRespuesta
                                     {
                                         campo = c,
                                         flujopasoxcampo = p
                                         

                                     }).ToList();

                    return Respuesta;
                }

                return ObjRespuesta;


            }
            catch (Exception)
            {

                throw;
            }

        }

    }
}
