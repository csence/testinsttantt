﻿using ApiFlujo.Domain.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using ApiFlujo.Domain.Dto;

namespace ApiFlujo.DataAccess
{
    public class FlujoDAL
    {
        private readonly ModelContext context;
        public FlujoDAL(ModelContext _context)
        {
            context = _context;
        }

        public  async Task<List<Flujos>>  ObtenerFlujos() {

            var Respuesta =  context.flujos.ToList();

            return Respuesta;
        }

        public async Task<List<FlujoPasosXCampo>> obtenerFlujoPasosXCampo()
        {
            try
            {
                var Respuesta = context.FlujoPasosXCampo.ToList();

                return Respuesta;
            }
            catch (Exception ex)
            {

                throw ex;
            }
         
        }
        public async Task<List<FlujoXPaso>> obtenerFlujoXPaso()
        {
            try
            {
                var Respuesta = context.flujoXpasos.ToList();

                return Respuesta;
            }
            catch (Exception)
            {

                throw;
            }
          
        }

        public async Task<List<DtoFlujoxPaso>> obtenerFlujoXPaso( int idflujo)
        {
            try
            {
                var Respuesta = (from p in context.flujos
                                 join e in context.flujoXpasos
                                 on p.Id equals e.IdFlujo
                                 join a in context.pasos
                                 on e.IdPaso equals a.Id
                                 where p.Id == idflujo
                                 select new DtoFlujoxPaso
                                 {
                                     flujo = p,
                                     flujoxpaso = e,
                                     paso = a

                                 }).ToList();

                return Respuesta;
            }
            catch (Exception)
            {

                throw;
            }

        }
    }
}
