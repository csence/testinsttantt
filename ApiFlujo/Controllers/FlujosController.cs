﻿using ApiFlujo.BusinessLogic;
using ApiFlujo.Domain.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiFlujo.Controllers
{

    [ApiController]
    [Route("Flujos")]
    public class FlujosController : ControllerBase
    {

        private readonly FlujoBL _FlujoBL;

        public FlujosController(FlujoBL bl)
        {
            _FlujoBL = bl;
        }

        [HttpGet]
        [Route("ObtenerFlujos")]
        public async Task<IActionResult> ObtenerFlujos()
        {
            try
            {
                var Listflujo = new List<Flujos>();

                var Respuesta = _FlujoBL.ObtenerFlujos();

                return Ok(Respuesta.Result);
            }
            catch (Exception ex)
            {

             return  BadRequest(ex.Message);
            }

           
        }


        [HttpGet]
        [Route("obtenerFlujoPasosXCampo")]
        public async Task<IActionResult> obtenerFlujoPasosXCampo()
        {
            try
            {
                var Listflujo = new List<Flujos>();

                var Respuesta = _FlujoBL.obtenerFlujoPasosXCampo();

                return Ok(Respuesta.Result);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }


        }

        [HttpGet]
        [Route("obtenerflujoXpasos")]
        public async Task<IActionResult> obtenerflujoXpasos()
        {
            try
            {
                var Listflujo = new List<Flujos>();

                var Respuesta = await _FlujoBL.obtenerflujoXpasos();

                return Ok(Respuesta);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }


        }


        [HttpGet]
        [Route("obtenerFlujoXPaso/{idflujo}")]
        public async Task<IActionResult> obtenerFlujoXPaso(int idflujo)
        {
            try
            {
                var Listflujo = new List<Flujos>();

                var Respuesta = await _FlujoBL.obtenerFlujoXPaso(idflujo);

                return Ok(Respuesta);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }


        }

        


    }
}
