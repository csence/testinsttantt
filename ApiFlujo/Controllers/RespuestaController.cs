﻿using ApiFlujo.BusinessLogic;
using ApiFlujo.Domain.Dto;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiFlujo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RespuestaController : ControllerBase
    {
        private readonly RespuestaBL _RespuestaBL;

        public RespuestaController(RespuestaBL bl)
        {
            _RespuestaBL = bl;
        }

        [HttpPost]
        [Route("ObtenerCampos")]
        public async Task<IActionResult> ObtenerCampos(DtoCampos obj)
        {
            try
            {
                var Respuesta = await _RespuestaBL.ObtenerCampos(obj);

                return Ok(Respuesta);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }


        }
        [HttpPost]
        [Route("GuardarRespuesta")]
        public async Task<IActionResult> GuardarRespuesta(DtoCampos obj)
        {
            try
            {
                var Respuesta = await _RespuestaBL.GuardarRespuesta(obj);

                return Ok(Respuesta);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }


        }

        
    }
}
