﻿using ApiFlujo.DataAccess;
using ApiFlujo.Domain.Dto;
using ApiFlujo.Domain.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApiFlujo.BusinessLogic
{
  public  class FlujoBL
    {
        private readonly FlujoDAL _FlujoDal;

        public FlujoBL(FlujoDAL bl)
        {
            _FlujoDal = bl;
        }

        public  async Task<List<Flujos>> ObtenerFlujos() {

            var Respuesta = await _FlujoDal.ObtenerFlujos();

            return Respuesta;
        }

        public async Task<List<FlujoPasosXCampo>> obtenerFlujoPasosXCampo()
        {
            var Respuesta = await _FlujoDal.obtenerFlujoPasosXCampo();

            return Respuesta;
        }
         
        public async Task<List<FlujoXPaso>> obtenerflujoXpasos()
        {

                var Respuesta = await _FlujoDal.obtenerFlujoXPaso();

                return Respuesta;
            }

        public async Task<List<DtoFlujoxPaso>> obtenerFlujoXPaso(int idflujo)
        {

            var Respuesta = await _FlujoDal.obtenerFlujoXPaso(idflujo);

            return Respuesta;
        }


    }
}
