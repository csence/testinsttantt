﻿using ApiFlujo.DataAccess;
using ApiFlujo.Domain.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApiFlujo.BusinessLogic
{
  public  class RespuestaBL
    {
        private readonly RespuestaDAL _RespuestaDAL;

        public RespuestaBL(RespuestaDAL bl)
        {
            _RespuestaDAL = bl;
        }

        public async Task<List<PasoXCampoXRespuesta>> ObtenerCampos(DtoCampos obj)
        {

            var Respuesta = await _RespuestaDAL.ObtenerCampos(obj);

            return Respuesta;
        }

        public async Task<List<PasoXCampoXRespuesta>> GuardarRespuesta(DtoCampos obj)
        {
            var Respuesta = await _RespuestaDAL.GuardarRespuesta(obj);

            return Respuesta;
        }

    }
}
